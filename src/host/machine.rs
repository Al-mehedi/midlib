use std::io;
use regex::Regex;
use std::process::Command as cmd;

#[derive(Debug)]
pub struct IP { address: Result<Vec<IpAddress>, io::Error> }

#[derive(Debug)]
enum IpAddress { V4(String), V6(String) }

impl IP {
    pub fn lookup() -> IP {
        IP { address: obtain_global_scope() }
    }
}

/*
    Obtains available IP addresses from the host machine.
*/
fn obtain_global_scope() -> Result<Vec<IpAddress>, io::Error> {
    let output = cmd::new("ip").args(&["addr", "show"]).output()?;
    let output = String::from_utf8(output.stdout).expect("Failed to parse string from output!");

    let mut list = Vec::new();

    let reg = Regex::new(r"[^\d:\s].+scope\sglobal").unwrap();
    for capture in reg.captures_iter(&output) {
        let string = capture.get(0).map_or("", |m| m.as_str());

        if string.contains("inet6") {
            let (_, string) = string.split_at(6);
            let address: Vec<&str> = string.split('/').collect();
            list.push(IpAddress::V6(String::from(address[0])));
        } else {
            let (_, string) = string.split_at(5);
            let address: Vec<&str> = string.split('/').collect();
            list.push(IpAddress::V4(String::from(address[0])));
        }
    }

    Ok(list)
}
