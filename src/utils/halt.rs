/*
    Halt's the current execution for user input.
    Equivalent to getch() function in C.
*/
pub fn now() {
    println!("Press enter to exit...");
    let mut exit_key = String::new();
    std::io::stdin().read_line(&mut exit_key).expect("STD input failed!");
}