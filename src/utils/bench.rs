use std::time::Instant;

/*
    Returns an instant corresponding to "now".
*/
pub fn start() -> Instant { Instant::now() }

/*
    Returns the amount of time elapsed since this instant was created.
*/
pub fn end(id: &str, time: Instant) { println!("{} took: {:?}", id, time.elapsed()); }